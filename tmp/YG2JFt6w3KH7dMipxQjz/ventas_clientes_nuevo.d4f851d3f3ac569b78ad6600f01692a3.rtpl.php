<?php if(!class_exists('raintpl')){exit;}?><form class="form-horizontal" role="form" name="f_nuevo_cliente" action="<?php echo $fsc->url();?>" method="post">
    <div class="modal" id="modal_nuevo_cliente">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <span class="glyphicon glyphicon-user"></span>
                        &nbsp; Nuevo cliente
                    </h4>
                    <p class="help-block">
                        Si quieres, puedes cambiar las
                        <a href="index.php?page=ventas_clientes_opciones">opciones para nuevos clientes</a>.
                    </p>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Código</label>
                        <div class="col-sm-10">
                            <input type="text" name="codigo" class="form-control" placeholder="automático" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" class="form-control" autocomplete="off" required=""/>
                        </div>
                    </div>
                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_cifnif_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label"><?php  echo FS_CIFNIF;?></label>
                        <div class="col-sm-3">
                            <select name="tipoidfiscal" class="form-control">
                                <?php $tiposid=$this->var['tiposid']=fs_tipos_id_fiscal();?>

                                <?php $loop_var1=$tiposid; $counter1=-1; if($loop_var1) foreach( $loop_var1 as $key1 => $value1 ){ $counter1++; ?>

                                <option value="<?php echo $value1;?>"><?php echo $value1;?></option>
                                <?php } ?>

                            </select>
                        </div>
                        <div class="col-sm-7">
                            <input type="text" name="cifnif" class="form-control" maxlength="30" autocomplete="off"<?php if( $fsc->nuevocli_setup['nuevocli_cifnif_req'] ){ ?> required=""<?php } ?>/>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="personafisica" value="TRUE" checked=""/> persona física (no empresa)
                            </label>
                        </div>
                    </div>
                    <?php if( $fsc->grupos ){ ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grupo</label>
                        <div class="col-sm-10">
                            <select name="scodgrupo" class="form-control">
                                <option value="">Ninguno</option>
                                <option value="">------</option>
                                <?php $loop_var1=$fsc->grupos; $counter1=-1; if($loop_var1) foreach( $loop_var1 as $key1 => $value1 ){ $counter1++; ?>

                                <?php if( $value1->codgrupo==$fsc->nuevocli_setup['nuevocli_codgrupo'] ){ ?>

                                <option value="<?php echo $value1->codgrupo;?>" selected=""><?php echo $value1->nombre;?></option>
                                <?php }else{ ?>

                                <option value="<?php echo $value1->codgrupo;?>"><?php echo $value1->nombre;?></option>
                                <?php } ?>

                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_telefono1'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_telefono1_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">Teléfono 1</label>
                        <div class="col-sm-10">
                            <input type="text" name="telefono1" class="form-control" autocomplete="off"<?php if( $fsc->nuevocli_setup['nuevocli_telefono1_req'] ){ ?> required=""<?php } ?>/>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_telefono2'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_telefono2_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Teléfono 2</label>
                        <div class="col-sm-10">
                            <input type="text" name="telefono2" class="form-control" autocomplete="off"<?php if( $fsc->nuevocli_setup['nuevocli_telefono2_req'] ){ ?> required=""<?php } ?>/>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_email'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_email_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" class="form-control" autocomplete="off"<?php if( $fsc->nuevocli_setup['nuevocli_email_req'] ){ ?> required=""<?php } ?>/>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_pais'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_pais_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">
                            <a href="<?php echo $fsc->pais->url();?>">País</a>
                        </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="pais">
                                <?php $loop_var1=$fsc->pais->all(); $counter1=-1; if($loop_var1) foreach( $loop_var1 as $key1 => $value1 ){ $counter1++; ?>

                                <option value="<?php echo $value1->codpais;?>"<?php if( $value1->is_default() ){ ?> selected=""<?php } ?>><?php echo $value1->nombre;?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_provincia'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_provincia_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label text-capitalize"><?php  echo FS_PROVINCIA;?></label>
                        <div class="col-sm-10">
                            <?php if( $fsc->nuevocli_setup['nuevocli_provincia_req'] ){ ?>

                            <input type="text" name="provincia" id="ac_provincia" class="form-control" autocomplete="off" required=""/>
                            <?php }else{ ?>

                            <input type="text" name="provincia" value="<?php echo $fsc->empresa->provincia;?>" id="ac_provincia" class="form-control" autocomplete="off"/>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_ciudad'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_ciudad_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">Ciudad</label>
                        <div class="col-sm-10">
                            <?php if( $fsc->nuevocli_setup['nuevocli_ciudad_req'] ){ ?>

                            <input type="text" name="ciudad" class="form-control" required=""/>
                            <?php }else{ ?>

                            <input type="text" name="ciudad" value="<?php echo $fsc->empresa->ciudad;?>" class="form-control"/>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_codpostal'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_codpostal_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">Cód. Postal</label>
                        <div class="col-sm-10">
                            <?php if( $fsc->nuevocli_setup['nuevocli_codpostal_req'] ){ ?>

                            <input type="text" name="codpostal" class="form-control" maxlength="10" autocomplete="off" required=""/>
                            <?php }else{ ?>

                            <input type="text" name="codpostal" class="form-control" maxlength="10" autocomplete="off"/>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_direccion'] ){ ?>

                    <?php $divClass=$this->var['divClass']='';?>

                    <?php if( $fsc->nuevocli_setup['nuevocli_direccion_req'] ){ ?><?php $divClass=$this->var['divClass']=' has-warning';?><?php } ?>

                    <div class="form-group<?php echo $divClass;?>">
                        <label class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-10">
                            <?php if( $fsc->nuevocli_setup['nuevocli_direccion_req'] ){ ?>

                            <input type="text" name="direccion" class="form-control" autocomplete="off" required=""/>
                            <?php }else{ ?>

                            <input type="text" name="direccion" class="form-control" autocomplete="off"/>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                    <p class="help-block">
                        Puedes importar, exportar o actualizar masivamente clientes usando el plugin
                        <a href="https://www.facturascripts.com/plugin/import_export_csv" target="_blank">
                            Importar/Exportar CSV
                        </a>
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp; Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>