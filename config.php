<?php
define('FS_DB_TYPE', 'MYSQL');
define('FS_DB_HOST', 'localhost');
define('FS_DB_PORT', '3306');
define('FS_DB_NAME', 'factu');
define('FS_DB_USER', 'factu');
define('FS_DB_PASS', 'factu');
define('FS_CACHE_HOST', 'localhost');
define('FS_CACHE_PORT', '11211');
define('FS_CACHE_PREFIX', 'tJBofG4T_');
define('FS_TMP_NAME', 'YG2JFt6w3KH7dMipxQjz/');
define('FS_COOKIES_EXPIRE', 604800);
define('FS_ITEM_LIMIT', 50);
define('FS_DB_HISTORY', FALSE);
define('FS_DEMO', FALSE);
define('FS_DISABLE_MOD_PLUGINS', FALSE);
define('FS_DISABLE_ADD_PLUGINS', FALSE);
define('FS_DISABLE_RM_PLUGINS', FALSE);
